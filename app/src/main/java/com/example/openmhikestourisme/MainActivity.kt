package com.example.openmhikestourisme

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.browser.customtabs.CustomTabsIntent

class MainActivity : AppCompatActivity() {

    val  builder =  CustomTabsIntent.Builder();
    val customTabsIntent = builder.build();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        findViewById<Button>(R.id.open_mhikes).setOnClickListener {
             var url1 = "mhikestourisme://openhike?reference=5fb56782570a8"

             customTabsIntent.launchUrl(applicationContext, Uri.parse(url1));
        }

        findViewById<Button>(R.id.open_hike_group).setOnClickListener {
             var url2 = "mhikestourisme://openkiosque?group=618&kiosk=94"

             customTabsIntent.launchUrl(applicationContext, Uri.parse(url2));
        }



        findViewById<Button>(R.id.opensearch).setOnClickListener {
             var url3 = "mhikestourisme://opensearch?fromsearch=true&searchKeywords=&activityType=16,1&Language=fr&difficulty=2&minDuration=4&maxDuration=18&criterias=586,587"
             customTabsIntent.launchUrl(applicationContext, Uri.parse(url3));
        }

        findViewById<Button>(R.id.openmhikes).setOnClickListener {
            var url1 = "mhikestourisme://openhike?"
             customTabsIntent.launchUrl(applicationContext, Uri.parse(url1));
        }

    }





}